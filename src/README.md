                   									DELILAH RESTO MANUAL DE USUARIO

	
												      URLS


LINK DE LA PAGINA PRINCIPAL: https://www.delilahresto.cf/index/

LINK SWAGGER: https://www.delilahresto.cf/api-docs/

FRONTEND REPO: https://gitlab.com/BONAFO08/delilah-resto-frontend-vanilla

BACKEND REPO: https://gitlab.com/BONAFO08/delilah-resto-app-backend-final


Versión actual del Proyecto: 

FRONTEND: Alfa 1.0 

BACKEND: 4.0 

@Nota importante: La interfaz (Frontend) aún no está terminada. Las siguientes funciones están actualmente implementadas:

-Registro de usuario
-Login de usuario (Google y Local).
-Visualización de productos.
-Visualización de carrito.
-Visualización de la información de la cuenta del usuario.
-Modificación de la información de la cuenta del usuario.
-Creación de un pedido.
-Confirmación de un pedido (Paypal y Local).
-Eliminación de la cuenta.

El resto de las funciones deberán ser utilizadas mediante la url indicada como “LINK SWAGGER”.

@Nota adicional: Para realizar acciones con una cuenta de Google en la página Swagger, por favor siga los siguientes pasos:

-Logearse en el “LINK DE LA PAGINA PRINCIPAL”.
-Abrir la consola de desarrollador (F12).
-Abrir la pestaña Application.
-Abrir la pestaña Session Storage.
-Abrir la pestaña con el nombre de la página.
-Copiar el contenido de la variable “token”.
-Utilizar dicho contenido para identificarse en las rutas.



												INSTALACION

-Instalar Docker y Docker Compose.
-Descargar el repositorio indicado como “FRONTEND REPO”.
-Completar el archivo “docker-compose.yml“ proporcionado con la siguiente información :

*MONGO_ATLAS_URL= [URL de MongoAtlas a utilizar] (Es necesario modificar la imagen docker para utilizar otra base de datos, o una base de datos local)
*PAYPAL_ID = [ID proporcionado por PayPal]
*PAYPAL_SECRET = Código secreto proporcionado por PayPal]
*JWT_SECRET= [Semilla o texto para la generación de contraseñas]
*CLIENT_ID=[ID proporcionado por Google]
*CLIENT_SECRET= [Código secreto proporcionado por Google]
*DOLLAR= [Valor actual de la conversión Dólar-Peso]

-Iniciar el servidor con el comando “sudo docker-compose up -d”


										            DEFAULT ADMINISTRADOR

- username: admin
- password: a1d2m3i4n5




