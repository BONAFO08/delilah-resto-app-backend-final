import "dotenv/config.js";
import  mongoose  from "mongoose";
import userSchema from "../models/user.model.js";
import productSchema from "../models/product.model.js";
import paySchema from "../models/pay.model.js";
import orderSchema from "../models/order.model.js";
import orderStandbySchema from "../models/order.orderCollecor.model.js";


const options = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
}


// ENV


const uri = process.env.MONGO_ATLAS_URL;

mongoose.connect(uri);

//MODELS
const User = mongoose.model('users', userSchema);
const Product = mongoose.model('products', productSchema);
const Pay = mongoose.model('payments', paySchema);
const Order = mongoose.model('orders', orderSchema);
const OrderStandby = mongoose.model('orderStandby', orderStandbySchema);


export {
    mongoose,
    User,
    Product,
    Pay,
    Order,
    OrderStandby,
}