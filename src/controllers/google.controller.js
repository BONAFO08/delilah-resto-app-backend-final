import "dotenv/config.js";
import { User } from "../config/conexion.config.js";
import jwt from 'jsonwebtoken';


//Create a key for the relation order - user
const generateKey = () => {
    const num = Math.floor(Math.random() * 9999999999) + 1;
    return num;
}

//Create an id for the front identify if user is admin or client
const generateSeissionID = (range) => {
    let num = Math.floor(Math.random() * 9999999999) + 1;
    let decoder = num % 512;
    if (range == 'admin') {
        num -= decoder
    } else {
        (num % 512 == 0) ? (num += 20) : ('')
    }
    return num
}



const findByGoogleId = async (bd, googleID) => {
    let aux;
    let result = await bd.find({ googleID: googleID })
        .then(resolve => {
            (resolve.length == 0) ? (aux = false) : (aux = resolve)
        });
    return aux;
};

const newTokensing = async (data) => {
    data._id = data._id.toString();
    const token = jwt.sign(data, process.env.JWT_SECRET, { expiresIn: '24h' });
    return token;

}

const loginGoogle = async (data) => {

    const validator = await findByGoogleId(User, data.googleID)



    if (validator == false) {

        const temp = await new User({
            username: `Butterfly${data.googleID}`,
            name: data.name,
            phone: 0,
            email: data.email,
            address: [],
            password: generateKey()+`Butterfly${data.googleID}`,
            range: 'client',
            state: false,
            ban: false,
            userKey: generateKey(),
            userImg: data.userImg,
            typesAaccount: 1,
            googleID: data.googleID,
            facebookID: null,
            linkedinID: null,
        }
        );

        const result = await temp.save();

        const tokenData = {
            _id: temp._id,
            range: temp.range,
        }
        const token = await newTokensing(tokenData)
        return { token: token, sessionID: generateSeissionID(temp.range), serImg: temp.userImg,};
    } else {
        const tokenData = {
            _id: validator[0]._id,
            range: validator[0].range,
        }
        const token = await newTokensing(tokenData)
        return { token: token, sessionID: generateSeissionID(validator[0].range), userImg: validator[0].userImg};
    }
};

export {
    loginGoogle
}