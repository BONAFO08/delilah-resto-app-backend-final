
import { showCacheProducts } from "./redis.product.js";
import { findById, findByuserKey } from "./search.js"
import { Order, User, Pay, Product } from "../config/conexion.config.js";


// Send the actual  Day / Month / Year  Hour : Minutes
const newDate = () => {
    let day = new Date();
    let month = new Date();
    let year = new Date();
    let hour = new Date();
    let min = new Date();
    let time = '';

    day = day.getDate(),
        month = month.getMonth() + 1,
        year = year.getFullYear(),
        hour = hour.getHours(),
        min = min.getMinutes()

    if (min <= 9) {
        min = `0${min}`;
    }

    if (hour <= 12) {
        time = `${hour}:${min} AM`;
    } else {
        time = `${hour - 12}:${min} PM`;
    }



    let fullDate = `${day}/${month}/${year} ${time}`;

    return fullDate;
};


//Search all products in an order in the product database
const findProduct = async (selectedFood) => {
    let arrFood = selectedFood;
    let allProducts = await showCacheProducts();
    let bill = 0;


    for (let i = 0; i < arrFood.length; i++) {

        let product = allProducts.filter(allProducts => allProducts._id == arrFood[i].id);

        (product.length == 0) ? (product[0] = { price: 'FAIL' }) : ('');

        (arrFood[i].ammount == 0) ? (arrFood[i].ammount = 1) : ('');
        bill = bill + (product[0].price * arrFood[i].ammount);
    }

    return bill
}

//Find repeated products for to modify the ammount in the order or delete a product in the order
const findRepeatProduct = (food) => {
    let arrFood = food;
    let arrFoodTemp = food;
    for (let i = 0; i < arrFood.length; i++) {
        let product = arrFoodTemp.filter(arrFoodTemp => arrFoodTemp.id == arrFood[i].id);
        if (product.length > 1) {
            if (product[1].ammount == 0) {
                arrFoodTemp = arrFoodTemp.filter(arrFoodTemp => arrFoodTemp.id != product[0].id);
                arrFood = arrFoodTemp;
            } else {
                product[0].ammount = product[1].ammount;
                arrFoodTemp = arrFoodTemp.filter(arrFoodTemp => arrFoodTemp.id != product[0].id);
                arrFood = arrFoodTemp;
                arrFood.push(product[0]);
            }
        }
    }
    return arrFood;
}


//Create a new order and save it in the database and the record of the user
const createOrder = async (order) => {
    let msj = {
        txt: '',
        status: 200,
    };
    const orderData = {};



    let dataUser = (await findById(User, order.userID))[0];

    if (dataUser != undefined) {
        const addressUser = dataUser.address[order.addressIndex - 1];

        (addressUser != undefined)
            ? (orderData.address = addressUser)
            : (msj.txt = 'Domicilio invalido', msj.status = 404);

        (dataUser.username != undefined)
            ? (orderData.username = dataUser.username)
            : (msj.txt = 'Nombre de usuario invalido', msj.status = 404);

        (dataUser.name != undefined)
            ? (orderData.name = dataUser.name)
            : (msj.txt = 'Nombre y Apellido invalido', msj.status = 404);

        (dataUser.phone != undefined)
            ? (orderData.phone = dataUser.phone)
            : (msj.txt = 'Telefono invalido', msj.status = 404);

        orderData.userKey = dataUser.userKey;

        if (msj.status == 404) {
            return msj
        }
    } else {
        msj.txt = 'El usuario no existe';
        msj.status = 404
        return msj

    }

    orderData.food = order.food;

    if (order.payID != undefined) {
        const pay = (await findById(Pay, order.payID))[0];
        if (pay == undefined) {
            msj.txt = 'El metodo de pago no existe'
            msj.status = 404
            return msj
        } else {
            orderData.pay = pay.name
        }
    } else {
        msj.txt = 'El metodo de pago no existe';
        msj.status = 404
        return msj
    }

    if (orderData.pay == 'Paypal') {
        if (order.paypalID != undefined) {
            orderData.paypalID = order.paypalID;
        } else {
            msj.txt = 'Paypal ID invalido.';
            msj.status = 404
            return msj
        }
    }


    if (msj.status == 200) {

        const date = newDate();

        const finalOrder = await new Order({
            username: orderData.username,
            name: orderData.name,
            address: orderData.address,
            phone: orderData.phone,
            pay: orderData.pay,
            stade: 'Confirmado',
            food: orderData.food,
            bill: order.bill,
            date: date,
            userKey: orderData.userKey,
            paypalID: orderData.paypalID,
        }
        );

        const result = await finalOrder.save();
        dataUser.reco.push(finalOrder);

        const userUpdated = await User.updateOne({ _id: dataUser._id },
            {
                $set: {
                    reco: dataUser.reco,
                }
            }
        );

        console.log(`[${new Date}] Order Creator : A new order has born! // ORDER_ID: ${finalOrder._id}`);
        msj.txt = 'Gracias por su pedido.'

    }
    console.log(`[${new Date}] Order Creator : ${msj.txt}`);
    return msj

}



// //Create a new order and save it in the database and the record of the user
// const createOrder = async (token, address, selectedFood, idPay, paypalID) => {
//     let msj = {
//         txt: '',
//         status: 200,
//     };

//     //User Data
//     let dataUser = await findById(User, token._id);
//     dataUser = dataUser[0];
//     let addressUser = dataUser.address[address - 1];

//     (addressUser != undefined) ? ('')
//         : (msj.txt = 'Domicilio invalido', msj.status = 404);

//     //Product Data
//     let arrFood = selectedFood;
//     let bill = await findProduct(arrFood);
//     (isNaN(bill) == true) ? (msj.txt = 'Uno o mas de los productos solicitados no existe', msj.status = 404) : ('');


//     //Pay Data
//     let pay = await findById(Pay, idPay);

//     (pay == false) ? (msj.txt = 'El metodo de pago no existe', msj.status = 404) : ('');

//     pay = pay[0];

//     if (msj.status == 200) {
//         //Order Data
//         let date = newDate();

//         const temp = await new Order({
//             username: dataUser.username,
//             name: dataUser.name,
//             address: addressUser,
//             phone: dataUser.phone,
//             pay: pay.name,
//             stade: 'Confirmado',
//             food: arrFood,
//             bill: bill,
//             date: date,
//             userKey: dataUser.userKey,
//             paypalID: paypalID,
//         }
//         );

//         const result = await temp.save();

//         dataUser.reco.push(temp);

//         const userUpdated = await User.updateOne({ _id: dataUser._id },
//             {
//                 $set: {
//                     reco: dataUser.reco,
//                 }
//             }
//         );
//         msj.txt = 'Gracias por su pedido.'
//     }
//     return msj;
// }

//Modify an order and update it in the database and the record of the user
const modifyOrder = async (token, address, selectedFood, idPay, idOrder) => {
    let msj = {
        txt: '',
        status: 200,
    };


    let dataUser = await findById(User, token._id);
    let userId = dataUser[0]._id.toString();
    let arrAddress = dataUser[0].address;
    dataUser = dataUser[0].reco;
    let dataOrder = dataUser.filter(dataUser => dataUser._id == idOrder);
    dataOrder = dataOrder[0];
    let orderIndex = dataUser.indexOf(dataOrder);


    if (dataOrder != undefined) {
        if (dataOrder.stade == 'Pendiente') {

            let validator = {
                address: '',
                arrFood: [],
                bill: 0,
                pay: '',
                auxFood: true
            };

            //User Data
            (address == false || arrAddress[address - 1] == undefined || dataOrder.address == arrAddress[address - 1])
                ? (validator.address = false)
                : (validator.address = arrAddress[address - 1]);

            //Product Data
            let arrFood = [];

            if (selectedFood == false) {
                validator.arrFood = false;
            } else {
                arrFood = dataOrder.food.concat(selectedFood);
                arrFood = findRepeatProduct(arrFood);
                validator.arrFood = arrFood
            }

            //Bill
            let bill = await findProduct(validator.arrFood);
            (isNaN(bill) == true) ? (validator.bill = false) : (validator.bill = bill);

            (validator.arrFood == false || validator.bill == false)
                ? (validator.auxFood = false)
                : ('');


            //Pay
            let pay = await findById(Pay, idPay);
            (pay == false || pay[0].name == dataOrder.pay) ? (validator.pay = false) : (validator.pay = pay[0].name);

            // Final validation
            let finalValidator = validator.address || validator.pay || validator.auxFood;

            if (finalValidator != false) {

                dataOrder.address = (validator.address == false) ? (dataOrder.address) : (validator.address);
                dataOrder.pay = (validator.pay == false) ? (dataOrder.pay) : (validator.pay);
                dataOrder.food = (validator.auxFood == false) ? (dataOrder.food) : (validator.arrFood);
                dataOrder.bill = (validator.auxFood == false) ? (dataOrder.bill) : (validator.bill);

                const orderUpdated = await Order.updateMany({ _id: idOrder },
                    {
                        $set: {
                            address: dataOrder.address,
                            pay: dataOrder.pay,
                            food: dataOrder.food,
                            bill: dataOrder.bill
                        }
                    }
                );

                dataUser[orderIndex] = dataOrder;

                const userUpdated = await User.updateOne({ _id: userId },
                    {
                        $set: {
                            reco: dataUser,
                        }
                    }
                );

                msj.txt = 'Pedido Modificado';
                msj.status = 200;
            } else {
                msj.txt = 'Lo siento pero todos los datos enviados son invalidos';
                msj.status = 400;
            }
        } else {
            msj.txt = 'No puedes modificar un pedido ya cerrado';
            msj.status = 403;
        }
    } else {
        msj.txt = 'Pedido no encontrado';
        msj.status = 404;
    }
    return msj;
}

//Cancel an order and update it in the database and the record of the user
const cancelateOrder = async (token, idOrder) => {
    let msj = {
        txt: '',
        status: 200,
    };

    let dataUser = await findById(User, token._id);
    let userId = dataUser[0]._id.toString();

    dataUser = dataUser[0].reco;
    let dataOrder = dataUser.filter(dataUser => dataUser._id == idOrder);
    dataOrder = dataOrder[0];
    let orderIndex = dataUser.indexOf(dataOrder);

    if (dataOrder != undefined) {
        if (dataOrder.stade != 'Entregado' && dataOrder.stade != 'Cancelado') {
            dataOrder.stade = 'Cancelado';
            const orderUpdated = await Order.updateOne({ _id: idOrder },
                {
                    $set: {
                        stade: dataOrder.stade
                    }
                }
            );

            dataUser[orderIndex] = dataOrder;

            const userUpdated = await User.updateOne({ _id: userId },
                {
                    $set: {
                        reco: dataUser,
                    }
                }
            );

            msj.txt = 'Pedido cancelado exitosamente';
            msj.status = 200;
        } else {
            msj.txt = 'No puedes cancelar un pedido ya entregado / cancelado';
            msj.status = 403;
        }
    } else {
        msj.txt = 'Pedido no encontrado';
        msj.status = 404;
    }
    return msj;
}

//Confirm an order and update it in the database and the record of the user
const confirmOrder = async (token, idOrder) => {
    let msj = {
        txt: '',
        status: 200,
    };

    let dataUser = await findById(User, token._id);
    let userId = dataUser[0]._id.toString();

    dataUser = dataUser[0].reco;
    let dataOrder = dataUser.filter(dataUser => dataUser._id == idOrder);
    dataOrder = dataOrder[0];
    let orderIndex = dataUser.indexOf(dataOrder);

    if (dataOrder != undefined) {
        if (dataOrder.stade == 'Pendiente') {
            dataOrder.stade = 'Confirmado';
            const orderUpdated = await Order.updateOne({ _id: idOrder },
                {
                    $set: {
                        stade: dataOrder.stade
                    }
                }
            );

            dataUser[orderIndex] = dataOrder;

            const userUpdated = await User.updateOne({ _id: userId },
                {
                    $set: {
                        reco: dataUser,
                    }
                }
            );

            msj.txt = 'Pedido confirmado exitosamente';
            msj.status = 200;
        } else {
            msj.txt = 'El pedido ya esta confirmado o en etapas posteriores';
            msj.status = 403;
        }
    } else {
        msj.txt = 'Pedido no encontrado';
        msj.status = 404;
    }
    return msj;
}

//Change the stade of an order and update it in the database and the record of the user
const ordenStade = async (idOrder, newStade) => {
    let msj = {
        txt: '',
        status: 200,
    };

    let dataOrder = await findById(Order, idOrder);
    dataOrder = dataOrder[0];


    let dataUser = await findByuserKey(User, dataOrder.userKey);
    let userId = dataUser[0]._id.toString();

    dataUser = dataUser[0].reco;


    let orderIndex = dataUser.filter(dataUser => dataUser._id == idOrder);
    orderIndex = dataUser.indexOf(orderIndex[0]);


    if (dataOrder != undefined) {
        dataOrder.stade = newStade;

        const orderUpdated = await Order.updateOne({ _id: idOrder },
            {
                $set: {
                    stade: dataOrder.stade
                }
            }
        );

        dataUser[orderIndex] = dataOrder;

        const userUpdated = await User.updateOne({ _id: userId },
            {
                $set: {
                    reco: dataUser,
                }
            }
        );

        msj.txt = 'Estado del pedido modificado exitosamente';
        msj.status = 200;
    } else {
        msj.txt = 'Pedido no encontrado';
        msj.status = 404;
    }
    return msj;
}

//Send all user orders
const userOrders = async (token) => {
    let msj = {
        txt: '',
        status: 200,
    };

    let dataUser = await findById(User, token._id);
    dataUser = dataUser[0].reco;

    let dataProducts = await Product.find()

    const tempOrder = dataUser.map(order => {
        delete order.userKey;
        // const foodTemp = order.food.map(food => {
        //     const foodData = dataProducts.filter(product => product._id == food.id);
        //     return {
        //         name: foodData[0].name,
        //         price: foodData[0].price,
        //         ammount: food.ammount
        //     }
        // })
        // order.food= foodTemp;
        return order
    })

    if (dataUser != undefined) {
        return tempOrder;
    } else {
        msj.txt = 'Usuario no encontrado';
        msj.status = 404;
    }
    return msj;
}


export {
    createOrder,
    modifyOrder,
    cancelateOrder,
    confirmOrder,
    ordenStade,
    userOrders,
    findProduct
};