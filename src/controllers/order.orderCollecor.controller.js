import { OrderStandby, User } from '../config/conexion.config.js'
import { findById, findByUsername } from './search.js';



const orderCollector = async (selectedFood, desToken) => {
   const userData = await findById(User, desToken._id)


   const orderData = await findByUsername(OrderStandby, userData[0].username)
   if (orderData == false) {
      const newOrder = await new OrderStandby({
         username: userData[0].username,
         order: selectedFood,
      }
      );
      const result = await newOrder.save();

      console.log(`[${new Date}] Order Collector : Created // ORDER_ID: ${newOrder._id}`);
      return 200
   } else {
      const oldOrder = JSON.stringify(selectedFood);
      const newOrder = JSON.stringify(orderData[0].order);
      if (oldOrder != newOrder) {
         const updateOrder = await OrderStandby.updateOne({ _id: orderData[0]._id },
            {
               $set: {
                  order: selectedFood,
               }
            }
         );
         console.log(`[${new Date}] Order Collector : Updated // ORDER_ID: ${orderData[0]._id}`);
         return 200

      }
   }
}


const orderLoader = async (desToken) => {
   const userData = await findById(User, desToken._id)
   const orderData = await findByUsername(OrderStandby, userData[0].username)


   if (orderData != false) {
      console.log(`[${new Date}] Order Loader : Load // ORDER_ID: ${orderData[0]._id}`);
      return {
         status  : 200,
         data: orderData[0]
      }
   }else{
      console.log(`[${new Date}] Order Loader : Nothing to Load`);
      return {
         status  : 404,
      }
   }
}

const orderEraser = async (desToken) => {
   const userData = await findById(User, desToken._id)
   const orderData = await findByUsername(OrderStandby, userData[0].username)

   if (orderData != false) {
       const deleteOrder = await OrderStandby.deleteOne({ _id: orderData[0]._id });
       console.log(`[${new Date}] Order Eraser : Deleted // ORDER_ID: ${orderData[0]._id}`);
       return {
           status: 200
       }
   } else {
       console.log(`[${new Date}] Order Eraser : Nothing to delete.`);
       return {
           status: 404
       }
   }
}


export {
   orderCollector,
   orderLoader,
   orderEraser

}