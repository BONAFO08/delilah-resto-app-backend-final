import 'dotenv/config.js'
import express from "express";
import paypal from "paypal-rest-sdk"
import { createOrder } from "../controllers/order.controller.js";
import { orderLoader } from '../controllers/order.orderCollecor.controller.js';
import { consumeToken } from "../controllers/user.controller.js";
const router = express.Router();

let ordersNoConfirms = [];


const calculateTotalImport = (order) => {
    const totalImport = order.reduce((previousValue, currentValue) =>
        previousValue + (currentValue.price * currentValue.ammount), 0);
    return totalImport
}



paypal.configure({
    'mode': 'sandbox',
    'client_id': process.env.PAYPAL_ID,
    'client_secret': process.env.PAYPAL_SECRET
});



router.post('/paypal', async (req, res) => {
    const desToken = consumeToken(req.body.token);
    const orderResponse = await orderLoader(desToken)
    let totalImport = calculateTotalImport(orderResponse.data.order);
    const totalImportARGpesos = totalImport;
    const addressIndex = parseInt(req.body.addressIndex);
    const payID = req.body.payID;
    totalImport = totalImport / process.env.DOLLAR;
    totalImport = totalImport.toFixed(2)

    // name: product.name,
    // price: `${tempPrice.toFixed(2)}`,
    // currency: "USD",
    // quantity: product.ammount,

    const order = orderResponse.data.order.map(product => {
        const tempPrice = product.price / process.env.DOLLAR;
        return {
            name: product.name,
            price: `${tempPrice.toFixed(2)}`,
            currency: "USD",
            quantity: product.ammount,
        }
    });

    if (orderResponse.status == 200) {
        const create_payment_json = {
            "intent": "sale",
            "payer": {
                "payment_method": "paypal"
            },
            "redirect_urls": {
                "return_url": `${process.env.BACK_END_URL}/success`,
                "cancel_url": `${process.env.BACK_END_URL}/cancel`
            },
            "transactions": [{
                "item_list": {
                    "items": order
                },
                "amount": {
                    "currency": "USD",
                    "total": totalImport
                },
                "description": "Gracias por su compra."
            }]
        };


        router.get('/success', (req, res) => {
            const payerId = req.query.PayerID;
            const paymentId = req.query.paymentId;

            const execute_payment_json = {
                "payer_id": payerId,
                "transactions": [{
                    "amount": {
                        "currency": "USD",
                        "total": totalImport
                    }
                }]
            };

            paypal.payment.execute(paymentId, execute_payment_json, async function (error, payment) {
                if (error) {
                    console.log('error paypal', error);
                    const frontUrl = `${process.env.FRONT_END_URL}/views/shop/orderError.html`;
                    res.redirect(frontUrl)
                    throw error;
                } else {
                    let paymentData = payment;
                    paymentData = JSON.stringify(paymentData);
                    paymentData = JSON.parse(paymentData);


                    const orderConfirmed =
                    {
                        userID: desToken._id,
                        addressIndex: addressIndex,
                        payID: payID,
                        stade: 'Confirmado',
                        food: orderResponse.data.order,
                        bill: totalImportARGpesos,
                        paypalID: paymentData.id

                    }

                    const response = await createOrder(orderConfirmed)
                    if (response.status == 200) {
                        const frontUrl = `${process.env.FRONT_END_URL}/views/shop/orderConfirmed.html`;
                        res.redirect(frontUrl)
                    } else {
                        const frontUrl = `${process.env.FRONT_END_URL}/views/shop/orderError.html`;
                        res.redirect(frontUrl)
                    }
                }
            });
        });


        paypal.payment.create(create_payment_json, function (error, payment) {
            if (error) {
                console.log('error paypal', error);
                const frontUrl = `${process.env.FRONT_END_URL}/views/shop/orderError.html`;
                res.redirect(frontUrl)
                throw error;
            } else {
                for (let i = 0; i < payment.links.length; i++) {
                    if (payment.links[i].rel === 'approval_url') {
                        res.redirect(payment.links[i].href);
                    }
                }
            }
        });




    } else {
        const frontUrl = `${process.env.FRONT_END_URL}/views/shop/orderError.html`;
        res.redirect(frontUrl)
        console.log(`[${new Date}] Order Creator : The user doesn't have an order!`);
    }
})


router.get('/cancel', (req, res) => {
    const frontUrl = `${process.env.FRONT_END_URL}/views/shop/orderCancel.html`;
    res.redirect(frontUrl)
    console.log(`[${new Date}] Order Creator : Paypal payment canceled`);


});



// router.post('/paypal', (req, res) => {
//     const order = ordersNoConfirms.filter(order => order.orderID == req.query.orderID)[0];
//     if (order == undefined) {
//         res.redirect(`${process.env.BACK_END_URL}/cancel`)
//     }



//     const create_payment_json = {
//         "intent": "sale",
//         "payer": {
//             "payment_method": "paypal"
//         },
//         "redirect_urls": {
//             "return_url": `${process.env.BACK_END_URL}/success?orderID=${req.query.orderID}`,
//             "cancel_url": `${process.env.BACK_END_URL}/cancel?orderID=${req.query.orderID}`
//         },
//         "transactions": [{
//             "item_list": {
//                 "items": order.arrFood
//             },
//             "amount": {
//                 "currency": "USD",
//                 "total": order.totalAmmout
//             },
//             "description": "Hat for the best team ever"
//         }]
//     };

//     router.get('/success', (req, res) => {
//         const payerId = req.query.PayerID;
//         const paymentId = req.query.paymentId;
//         const order = ordersNoConfirms.filter(order => order.orderID == req.query.orderID)[0];



//         const execute_payment_json = {
//             "payer_id": payerId,
//             "transactions": [{
//                 "amount": {
//                     "currency": "USD",
//                     "total": order.totalAmmout
//                 }
//             }]
//         };

//         paypal.payment.execute(paymentId, execute_payment_json, function (error, payment) {
//             if (error) {
//                 console.log(error.response);
//                 throw error;
//             } else {
//                 const order = ordersNoConfirms.filter(order => order.orderID == req.query.orderID)[0];
//                 ordersNoConfirms = ordersNoConfirms.filter(order => order.orderID != req.query.orderID);

//                 createOrder(order.token, order.address, order.idsProducts, order.idPay, payment.id)
//                     .then(resolve =>  res.status(200).redirect(`${process.env.FRONT_END_URL}/sucessPaypal`))
//                     .catch(error => res.send(error));


//             }
//         });
//     });

//     paypal.payment.create(create_payment_json, function (error, payment) {
//         if (error) {
//             throw error;
//         } else {
//             for (let i = 0; i < payment.links.length; i++) {
//                 if (payment.links[i].rel === 'approval_url') {
//                     res.redirect(payment.links[i].href);
//                 }
//             }
//         }
//     });

// });

// router.get('/cancel', (req, res) => {
//     const order = ordersNoConfirms.filter(order => order.orderID == req.query.orderID)[0];
//     ordersNoConfirms = ordersNoConfirms.filter(order => order.orderID != req.query.orderID);
//     console.log(ordersNoConfirms);
//     res.redirect(`${process.env.FRONT_END_URL}/cancelPaypal`)
// });




// let cosa ={"id":"PAYID-MKYNQAQ3A631249K2945342K","intent":"sale","state":"approved","cart":"19510051GH390222W","payer":{"payment_method":"paypal","status":"VERIFIED","payer_info":{"email":"sb-ritpy13861537@personal.example.com","first_name":"John","last_name":"Doe","payer_id":"GZY9ECY4AXYRY","shipping_address":{"recipient_name":"John Doe","line1":"Free Trade Zone","city":"Buenos Aires","state":"Buenos Aires","postal_code":"B1675","country_code":"AR"},"country_code":"AR"}},"transactions":[{"amount":{"total":"3.04","currency":"USD","details":{"subtotal":"3.04","shipping":"0.00","insurance":"0.00","handling_fee":"0.00","shipping_discount":"0.00","discount":"0.00"}},"payee":{"merchant_id":"BUJAEDVDCP2PA","email":"sb-c9bbx13864427@business.example.com"},"description":"Gracias por su compra.","item_list":{"items":[{"name":"Pizza de cuatro quesos","price":"1.30","currency":"USD","tax":"0.00","quantity":1},{"name":"Lomito","price":"1.74","currency":"USD","tax":"0.00","quantity":1}],"shipping_address":{"recipient_name":"John Doe","line1":"Free Trade Zone","city":"Buenos Aires","state":"Buenos Aires","postal_code":"B1675","country_code":"AR"}},"related_resources":[{"sale":{"id":"82760877A29969803","state":"completed","amount":{"total":"3.04","currency":"USD","details":{"subtotal":"3.04","shipping":"0.00","insurance":"0.00","handling_fee":"0.00","shipping_discount":"0.00","discount":"0.00"}},"payment_mode":"INSTANT_TRANSFER","protection_eligibility":"ELIGIBLE","protection_eligibility_type":"ITEM_NOT_RECEIVED_ELIGIBLE,UNAUTHORIZED_PAYMENT_ELIGIBLE","transaction_fee":{"value":"0.46","currency":"USD"},"parent_payment":"PAYID-MKYNQAQ3A631249K2945342K","create_time":"2022-06-20T20:27:00Z","update_time":"2022-06-20T20:27:00Z","links":[{"href":"https://api.sandbox.paypal.com/v1/payments/sale/82760877A29969803","rel":"self","method":"GET"},{"href":"https://api.sandbox.paypal.com/v1/payments/sale/82760877A29969803/refund","rel":"refund","method":"POST"},{"href":"https://api.sandbox.paypal.com/v1/payments/payment/PAYID-MKYNQAQ3A631249K2945342K","rel":"parent_payment","method":"GET"}]}}]}],"failed_transactions":[],"create_time":"2022-06-20T20:26:42Z","update_time":"2022-06-20T20:27:00Z","links":[{"href":"https://api.sandbox.paypal.com/v1/payments/payment/PAYID-MKYNQAQ3A631249K2945342K","rel":"self","method":"GET"}],"httpStatusCode":200};
// cosa = JSON.stringify(cosa);
// cosa = JSON.parse(cosa);
// const cosa2 = 
// {
//     userID: desToken,
//     addressIndex: 1, // FALTA
//     pay: payment.payer.payment_method,
//     stade : 'Confirmado',
//     food: orderResponse.data.order,
//     bill : totalImport,
//     paypalID: payment.id

// }
// username: String,
// name: String,
// address: String,
// phone: Number,
// date: String,
// userKey : Number,
// paypalID : String,



export { router }
