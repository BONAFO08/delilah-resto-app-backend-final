import express from "express";
import { cleaningFood } from "../controllers/dataVerify.js";
import { collectOrderMiddle, eraserOrderMiddle, loaderOrderMiddle } from "../middle/order.orderCollecor.middle.js";
const router = express.Router();




/**
 * @swagger
 * /orderCollector/saveOrder:
 *  post:
 *    tags: 
 *      [Collector]
 *    summary: Colector de pedidos 
 *    description: Recolecta los pedidos a confirmar por otras pasarelas de pago y los almacena en una base de datos 
 *    parameters:
 *    - name: authorization
 *      description: Token de usuario
 *      in: header
 *      required: false
 *      type: string
 *    - name: arrFood
 *      description: Comida seleccionada [Requiere Id del producto para modificar cantidad o agregar uno nuevo y ammount para mofificar la cantidad de este o ammount = 0 para eliminarlo del pedido]
 *      in: formData
 *      required: false
 *      type: array
 *      items: 
 *        type: object
 *        properties: 
 *          id: 
 *             type: string
 *          ammount: 
 *             type: integer
 *          name: 
 *             type: string
 *          price: 
 *             type: integer
 *    responses:
 *            200:
 *                description: Pedido almacenado exitosamente
 *            403:
 *                description: Credenciales incorrectas
 *            400:
 *                description: Error al validar los datos 
 */



router.post('/orderCollector/saveOrder', (req, res) => {
    collectOrderMiddle(req, res);
});



/**
 * @swagger
 * /orderCollector/loadOrder:
 *  get:
 *    tags: 
 *      [Collector]
 *    summary: Recuperador de pedidos 
 *    description: Si existe, envia el pedido sin confirmar almacenado de un usuario 
 *    parameters:
 *    - name: authorization
 *      description: Token de usuario
 *      in: header
 *      required: false
 *      type: string
 *    responses:
 *            200:
 *                description: Pedido enviado exitosamente
 *            403:
 *                description: Credenciales incorrectas
 *            400:
 *                description: Error al validar los datos 
 *            404:
 *                description: Pedido no encontrado
 */



 router.get('/orderCollector/loadOrder', (req, res) => {
    loaderOrderMiddle(req, res);
});


/**
 * @swagger
 * /orderCollector/deleteOrder:
 *  delete:
 *    tags: 
 *      [Collector]
 *    summary: Eliminador de pedidos
 *    description: Si existe, elimina el pedido sin confirmar almacenado de un usuario 
 *    parameters:
 *    - name: authorization
 *      description: Token de usuario
 *      in: header
 *      required: false
 *      type: string
 *    responses:
 *            200:
 *                description: Pedido eliminado exitosamente
 *            403:
 *                description: Credenciales incorrectas
 *            400:
 *                description: Error al validar los datos 
 *            404:
 *                description: Pedido no encontrado
 */



 router.delete('/orderCollector/deleteOrder', (req, res) => {
    eraserOrderMiddle(req, res);
});

export { router }
