import 'dotenv/config.js'
import  express  from "express";
const router = express.Router();
import passport from 'passport'
import GoogleStrategy from 'passport-google-oauth2';
import { loginWithGoogle } from '../middle/google.middle.js';

let docum = [];



passport.serializeUser((user, cb) => {
    cb(null, user)
})

passport.deserializeUser((user, cb) => {
    cb(null, user)
})

passport.use(new GoogleStrategy({
    clientID: process.env.CLIENT_ID,
    clientSecret: process.env.CLIENT_SECRET,
    callbackURL:  `${process.env.BACK_END_URL}/auth/google/callback`,
    passReqToCallback: true,
    scope: ['email', 'profile']
},
    function (request, accessToken, refreshToken, profile, done) {
        return done(null, profile);
    }
));


router.get('/auth/google/failure', (req, res) => {
    res.send('fail')
});

router.get('/auth/google',
    passport.authenticate('google', {
        scope:
            ['email', 'profile']
    }
    ));

router.get('/auth/google/callback',
    passport.authenticate('google', {
        failureRedirect: '/auth/google/failure'
    }), (req, res) => {


        loginWithGoogle(req,res)
    });








export {router}

