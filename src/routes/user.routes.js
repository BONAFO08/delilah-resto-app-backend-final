

import express from "express";
import { delAddress, delUser, logIn, logOut, modAddress, modImage, modPassword, modUser, newAddress, newUser, showAddress, showDataUser, validateToken } from '../middle/user.middle.js'
const router = express.Router();


/**
 * @swagger
 * /user/signUp:
 *  post:
 *    tags: 
 *      [User]
 *    summary: Crear usuario
 *    description: Crea un usuario y lo almacena en la coleccion Users
 *    parameters:
 *    - name: username
 *      description: Nickname del usuario
 *      in: formData
 *      required: false
 *      type: string
 *    - name: name
 *      description: Nombre y apellido del usuario
 *      in: formData
 *      required: false
 *      type: string
 *    - name: phone
 *      description: Teléfono del usuario
 *      in: formData
 *      required: false
 *      type: integer
 *    - name: email
 *      description: Correo electrónico del usuario
 *      in: formData
 *      required: false
 *      type: string
 *    - name: address
 *      description: Domicilio del usuario
 *      in: formData
 *      required: false
 *      type: string
 *    - name: password
 *      description: Contraseña del usuario
 *      in: formData
 *      required: false
 *      type: string
 *    - name: userImg
 *      description: Url de la imagen de perfil
 *      in: formData
 *      required: false
 *      type: string
 *    responses:
 *            200:
 *                description: Usuario creado exitosamente
 *            400:
 *                description: Error al validar los datos 
 *            409:
 *                description: El nombre de usuario y/o el correo electrónico ya existe
 * 
 */


router.post("/user/signUp", (req, res) => {
    newUser(req, res);
});


router.post("/user/validateToken", (req, res) => {
    validateToken(req,res)
});



/**
 * @swagger
 * /user/logIn:
 *  post:
 *    tags: 
 *      [User]
 *    summary: Loguear usuario
 *    description: Cambia el estado de un usuario de "false" a "true"
 *    parameters:
 *    - name: name
 *      description: Nickname del usuario
 *      in: formData
 *      required: false
 *      type: string
 *    - name: password
 *      description: Contraseña del usuario
 *      in: formData
 *      required: false
 *      type: string
 *    responses:
 *            200:
 *                description: Usuario logeado exitosamente
 *            403:
 *                description: Credenciales incorrectas
 *            400:
 *                description: Error al validar los datos  
 */


router.post("/user/logIn", (req, res) => {
    logIn(req, res);
});

/**
 * @swagger
 * /user/logout:
 *  get:
 *    tags: 
 *      [User]
 *    summary: Desloguear usuario
 *    description: Cambia el estado de un usuario de "true" a "false"
 *    parameters:
 *    - name: authorization
 *      description: Token de usuario
 *      in: header
 *      required: false
 *      type: string
 *    responses:
 *            200:
 *                description: Usuario deslogeado exitosamente
 *            403:
 *                description: Credenciales incorrectas
 *            400:
 *                description: Error al validar los datos 
 */


router.get("/user/logout", (req, res) => {
    logOut(req,res);
});

/**
 * @swagger
 * /user/showDataUser:
 *  get:
 *    tags: 
 *      [User]
 *    summary: Mostrar datos de usuario
 *    description: Muestra la data de un usuario
 *    parameters:
 *    - name: authorization
 *      description: Token de usuario
 *      in: header
 *      required: false
 *      type: string
 *    responses:
 *            200:
 *                description: Muestra los datos del usuario
 *            403:
 *                description: Credenciales incorrectas
 *            404:
 *                description: Usuario no encontrado
 */


 router.get("/user/showDataUser", (req, res) => {
    showDataUser(req, res);
});


/**
 * @swagger
 * /user/modUser:
 *  put:
 *    tags: 
 *      [User]
 *    summary: Modificar datos de Usuario
 *    description: Modifica los datos de perfil de un usuario
 *    parameters:
 *    - name: authorization
 *      description: Token de usuario
 *      in: header
 *      required: false
 *      type: string
 *    - name: username
 *      description: Nickname del usuario
 *      in: formData
 *      required: false
 *      type: string
 *    - name: name
 *      description: Nombre y apellido del usuario
 *      in: formData
 *      required: false
 *      type: string
 *    - name: phone
 *      description: Teléfono del usuario
 *      in: formData
 *      required: false
 *      type: string
 *    - name: email
 *      description: Correo electrónico del usuario
 *      in: formData
 *      required: false
 *      type: string
 *    - name: password
 *      description: Contraseña del usuario
 *      in: formData
 *      required: false
 *      type: string
 *    responses:
 *            200:
 *                description: Usuario modificado exitosamente
 *            403:
 *                description: Credenciales incorrectas
 *            400:
 *                description: Error al validar los datos 
 *            409:
 *                description: El nombre de usuario y/o el correo electrónico ya existe
 * 
 */


 router.put("/user/modUser", (req, res) => {
    modUser(req, res);
});



/**
 * @swagger
 * /user/changePassword:
 *  post:
 *    tags: 
 *      [User]
 *    summary: Cambiar contraseña
 *    description: Modifica la contraseña de un usuario
 *    parameters:
 *    - name: authorization
 *      description: Token de usuario
 *      in: header
 *      required: false
 *      type: string
 *    - name: oldPassword
 *      description: Contraseña actual
 *      in: formData
 *      required: false
 *      type: string
 *    - name: newPassword
 *      description: Nueva contraseña
 *      in: formData
 *      required: false
 *      type: string
 *    responses:
 *            200:
 *                description: Contraseña modificada exitosamente
 *            403:
 *                description: Credenciales incorrectas
 *            400:
 *                description: Error al validar los datos 
 * 
 */


 router.post("/user/changePassword", (req, res) => {
    modPassword(req, res);
});


/**
 * @swagger
 * /user/changePassword:
 *  post:
 *    tags: 
 *      [User]
 *    summary: Cambiar imagen de perfil
 *    description: Modifica la imagen de perfil de un usuario
 *    parameters:
 *    - name: authorization
 *      description: Token de usuario
 *      in: header
 *      required: false
 *      type: string
 *    - name: userImg
 *      description: URL de la nueva imagen
 *      in: formData
 *      required: false
 *      type: string
 *    responses:
 *            200:
 *                description: Imagen modificada exitosamente
 *            403:
 *                description: Credenciales incorrectas
 *            400:
 *                description: Error al validar los datos 
 * 
 */


 router.put("/user/changeUserImg", (req, res) => {
    modImage(req, res);
});





/**
 * @swagger
 * /user/deleteUser:
 *  delete:
 *    tags: 
 *      [User]
 *    summary: Eliminar Usuario
 *    description: Elimina un usuario de la Base de Datos
 *    parameters:
 *    - name: authorization
 *      description: Token de usuario
 *      in: header
 *      required: false
 *      type: string
 *    responses:
 *            200:
 *                description: Usuario eliminado exitosamente
 *            403:
 *                description: Credenciales incorrectas
 *            400:
 *                description: Error al validar los datos 
 * 
 */


 router.delete("/user/deleteUser", (req, res) => {
    delUser(req,res);
});


/**
 * @swagger
 * /user/showAddress:
 *  get:
 *    tags: 
 *      [Address]
 *    summary: Mostrar agenda de usuario
 *    description: Muestra toda la agenda de domicilios del usuario
 *    parameters:
 *    - name: authorization
 *      description: Token de usuario
 *      in: header
 *      required: false
 *      type: string
 *    responses:
 *            200:
 *                description: Se muestran todos los domicilios almacenados en la agenda del usuario
 *            403:
 *                description: Credenciales incorrectas
 *            404:
 *                description: Usuario no encontrado
 *            400:
 *                description: Error al validar los datos 
 * 
 */

//[DREP]
 router.get("/user/showAddress", (req, res) => {
    showAddress(req, res);
});

/**
 * @swagger
 * /user/newAddress:
 *  post:
 *    tags: 
 *      [Address]
 *    summary: Crear nuevo domicilio
 *    description: Crea un nuevo domicilio y lo almacena en la agenda del usuario
 *    parameters:
 *    - name: authorization
 *      description: Token de usuario
 *      in: header
 *      required: false
 *      type: string
 *    - name: address
 *      description: Nuevo domicilio
 *      in: formData
 *      required: false
 *      type: string
 *    responses:
 *            200:
 *                description: Domicilio creado exitosamente
 *            403:
 *                description: Credenciales incorrectas
 *            404:
 *                description: Usuario no encontrado
 *            409:
 *                description: El domicilio ya existe en la agenda
 * 
 */


 router.post("/user/newAddress", (req, res) => {
    newAddress(req, res);
});

/**
 * @swagger
 * /user/modAddress:
 *  put:
 *    tags: 
 *      [Address]
 *    summary: Modificar un domicilio
 *    description: Modificar un domicilio almacenado en la agenda del usuario
 *    parameters:
 *    - name: authorization
 *      description: Token de usuario
 *      in: header
 *      required: false
 *      type: string
 *    - name: oldAdr
 *      description: Numeración del domicilio a modificar dentro de la agenda del usuario  (Num Min  1)
 *      in: formData
 *      required: false
 *      type: string
 *    - name: address   
 *      description: Nuevo domicilio
 *      in: formData
 *      required: false
 *      type: string
 *    responses:
 *            200:
 *                description: Domicilio modificado exitosamente
 *            403:
 *                description: Credenciales incorrectas
 *            404:
 *                description: Usuario no encontrado o Domicilio a cambiar no encontrado
 *            400:
 *                description: Error al validar los datos 
 * 
 */


 router.put("/user/modAddress", (req, res) => {
    modAddress(req, res);
});

/**
 * @swagger
 * /user/delAddress:
 *  delete:
 *    tags: 
 *      [Address]
 *    summary: Eliminar un domicilio
 *    description: Eliminar un domicilio almacenado en la agenda del usuario
 *    parameters:
 *    - name: authorization
 *      description: Token de usuario
 *      in: header
 *      required: false
 *      type: string
 *    - name: oldAdr
 *      description: Numeración del domicilio a eliminar dentro de la agenda del usuario  (Num Min  1)
 *      in: formData
 *      required: false
 *      type: string
 *    responses:
 *            200:
 *                description: Domicilio eliminado exitosamente
 *            403:
 *                description: Credenciales incorrectas
 *            404:
 *                description: Usuario no encontrado o Domicilio a eliminar no encontrada
 *            400:
 *                description: Error al validar los datos 
 * 
 */


 router.delete("/user/delAddress", (req, res) => {
    delAddress(req, res);
});

export { router }
