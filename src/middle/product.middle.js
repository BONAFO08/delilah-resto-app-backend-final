import { validateProductData } from "../controllers/dataVerify.js";
import { showCacheProducts } from "../controllers/redis.product.js";
import { validateAdmin } from '../controllers/user.controller.js'
import { createProduct, deleteProduct, modifyAProduct } from '../controllers/product.controller.js'

//Show all products (in database and cache)
const showProductDB = (req, res) => {
    let desToken = validateAdmin(req.headers.authorization);
    if (desToken != false) {
        showCacheProducts().
            then(resolve => res.status(200).json(resolve))
            .catch(err => res.status(400).send(err));
    } else {
        res.status(403).send('Token invalido o expirado');
    }
}


//Create a new product
const newProduct = (req, res) => {
    let desToken = validateAdmin(req.headers.authorization);
    if (desToken != false) {
        if (desToken.range == 'admin') {
            let product = validateProductData(req.body, '', 'new')
            if (product.boolean == true) {
                createProduct(product.data)
                    .then(resolve => res.status(200).json({ msj: resolve, status: 200 }))
                    .catch(err => res.status(409).json({ msj: err, status: 409 }));
            } else {
                res.status(400).json({ msj: 'Lo siento.Has enviado datos invalidos.', status: 400 });
            }
        } else {
            res.status(403).json({ msj: 'Lo siento, no tienes permiso para acceder a este contenido.', status: 403 });
        }
    } else {
        res.status(403).json({ msj: 'Token invalido o expirado', status: 403 });
    }

}

//Modify a product
const modProduct = async (req, res) => {

    let desToken = validateAdmin(req.headers.authorization);

    if (desToken != false) {
        if (desToken.range == 'admin') {
            req.body['id'] = req.query.id;
            let product = validateProductData(req.body, '', 'update')
            let validator = product.name || product.price;
            let validator2 = product.id;
            if (validator != false && validator2 != false) {
                let response = await modifyAProduct(product, product.id);
                res.status(response.status).json({ msj: response.txt, status: response.status })
            } else {
                res.status(400).json({ msj: 'Lo siento.Has enviado datos invalidos.', status: 400 });
            }
        } else {
            res.status(403).json({ msj: 'Lo siento, no tienes permiso para acceder a este contenido.', status: 403 });
        }
    } else {
        res.status(403).json({ msj: 'Token invalido o expirado', status: 403 });
    }
};



//Delete a product
const delProduct = async (req, res) => {

    let desToken = validateAdmin(req.headers.authorization);

    if (desToken != false) {
        if (desToken.range == 'admin') {
            let product = validateProductData(req.query, '', 'update');
            let validator = product.id;
            if (validator != false) {
                let response = await deleteProduct(product.id);
                res.status(response.status).json({ msj: response.txt, status: response.status });
            } else {
                res.status(400).json({ msj: 'Lo siento.Has enviado datos invalidos.', status: 400 });
            }
        } else {
            res.status(403).json({ msj: 'Lo siento, no tienes permiso para acceder a este contenido.', status: 403 });
        }
    } else {
        res.status(403).json({ msj: 'Token invalido o expirado', status: 403 });
    }
};

export {
    showProductDB,
    newProduct,
    modProduct,
    delProduct
};