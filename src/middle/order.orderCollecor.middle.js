// import { OrderStandby } from '../config/conexion.config.js'
import { cleaningFood } from '../controllers/dataVerify.js';
import { orderCollector, orderEraser, orderLoader } from '../controllers/order.orderCollecor.controller.js';
import { validateAdmin } from '../controllers/user.controller.js'

const collectOrderMiddle = async (req, res) => {

    let desToken = validateAdmin(req.headers.authorization);
    if (desToken != false) {
        let selectedFood = cleaningFood(req.body.arrFood);
        let response = await orderCollector(selectedFood, desToken);
        (response == 200)
            ? (res.status(200).json({ msj: 'SAVED!', status: 200 }))
            : (res.status(400).json({ msj: 'BAD REQUEST!', status: 400 }));

    } else {
        res.status(403).json({ msj: 'Token invalido o expirado', status: 403 });
    }

}

const loaderOrderMiddle = async( req , res)=>{
    let desToken = validateAdmin(req.headers.authorization);
    if (desToken != false) {
        let response = await orderLoader( desToken);
        (response.status == 200)
            ? (res.status(200).json({ data : response.data.order, status: 200 }))
            : (res.status(400).json({ data: 'BAD REQUEST!', status: 400 }));

    } else {
        res.status(403).json({ msj: 'Token invalido o expirado', status: 403 });
    }
}

const eraserOrderMiddle = async( req , res)=>{
    let desToken = validateAdmin(req.headers.authorization);
    if (desToken != false) {
        let response = await orderEraser(desToken);
        (response.status == 200)
            ? (res.status(200).json({ data : 'ELIMINATED!', status: 200 }))
            : (res.status(400).json({ data: 'BAD REQUEST!', status: 400 }));

    } else {
        res.status(403).json({ msj: 'Token invalido o expirado', status: 403 });
    }
}

export {
    collectOrderMiddle,
    loaderOrderMiddle,
    eraserOrderMiddle

}