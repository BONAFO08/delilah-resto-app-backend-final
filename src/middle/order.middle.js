import { validateAdmin } from '../controllers/user.controller.js'
import { cleaningFood, validateFood, validateAddress, validatePayData, validateOrderStade } from '../controllers/dataVerify.js'
import { createOrder, modifyOrder, cancelateOrder, confirmOrder, ordenStade, userOrders, findProduct } from '../controllers/order.controller.js'
import { Order } from '../config/conexion.config.js'


//Create a new order
const newOrder = async (req, res) => {
    let desToken = validateAdmin(req.headers.authorization);
    const finalOrder = {};
    if (desToken != false) {
        let selectedFood = cleaningFood(req.body.arrFood);
        selectedFood = validateFood(selectedFood);
        if (selectedFood != false) {
            
            finalOrder.food = selectedFood;
            finalOrder.userID = desToken._id;
            finalOrder.payID = req.body.payID;
            finalOrder.bill = await findProduct(selectedFood);
            finalOrder.addressIndex = req.body.addressIndex;
            console.log(finalOrder);
            if(!isNaN(finalOrder.bill)){
                let response = await createOrder(finalOrder);
                res.status(response.status).json({ msj: response.txt, status: response.status });
            }else{
                res.status(400).json({ msj: 'Lo siento.Haz enviado datos invalidos.', status: 400 });
            }
        } else {
            res.status(400).json({ msj: 'Lo siento.Haz enviado datos invalidos.', status: 400 });
        }
    } else {
        res.status(403).json({ msj: 'Token invalido o expirado', status: 403 });
    }
}

//Modify an order
const modOrder = async (req, res) => {
    let desToken = validateAdmin(req.headers.authorization);
    if (desToken != false) {
        let selectedFood = cleaningFood(req.body.arrFood);
        selectedFood = validateFood(selectedFood);
        let selectedAddress = validateAddress(req.body.address);
        let idPay = { id: req.query.idPay };
        idPay = validatePayData(idPay, '', 'update');
        let idOrder = { id: req.query.idOrder };
        idOrder = validatePayData(idOrder, '', 'update');

        if ((selectedFood != false || idPay.id != false || selectedAddress != false) && idOrder.id != false) {
            let response = await modifyOrder(desToken, selectedAddress, selectedFood, idPay.id, idOrder.id);
            res.status(response.status).send(response.txt);
        } else {
            res.status(400).send('Lo siento.Haz enviado datos invalidos.');
        }
    } else {
        res.status(403).send('Token invalido o expirado');
    }
}



//Cancel an order
const cancelOrder = async (req, res) => {
    let desToken = validateAdmin(req.headers.authorization);
    if (desToken != false) {
        let idOrder = { id: req.query.idOrder };
        idOrder = validatePayData(idOrder, '', 'update');

        if (idOrder.id != false) {
            let response = await cancelateOrder(desToken, idOrder.id);
            res.status(200).json({ msj: response.txt, status: response.status });
        } else {
            res.status(400).json({ msj: 'Lo siento.Haz enviado datos invalidos.', status: 400 });
        }
    } else {
        res.status(403).json({ msj: 'Token invalido o expirado', status: 403 });
    }
}


//Confirm an order
const confOrder = async (req, res) => {
    let desToken = validateAdmin(req.headers.authorization);
    if (desToken != false) {
        let idOrder = { id: req.query.idOrder };
        idOrder = validatePayData(idOrder, '', 'update');

        if (idOrder.id != false) {
            let response = await confirmOrder(desToken, idOrder.id);
            res.status(response.status).send(response.txt);
        } else {
            res.status(400).send('Lo siento.Haz enviado datos invalidos.');
        }
    } else {
        res.status(403).send('Token invalido o expirado');
    }
}

//Change the stade of an order
const changeStadeOrder = async (req, res) => {
    let desToken = validateAdmin(req.headers.authorization);
    if (desToken != false) {
        if (desToken.range == 'admin') {
            let idOrder = { id: req.query.idOrder };
            idOrder = validatePayData(idOrder, '', 'update');

            let validator = validateOrderStade(req.query.newStade);
            if (idOrder.id != false && validator != false) {
                let response = await ordenStade(idOrder.id, req.query.newStade);
                res.status(response.status).json({ msj: response.txt, status: response.status });
            } else {
                res.status(400).json({ msj: 'Lo siento.Haz enviado datos invalidos.', status: 400 });
            }
        } else {
            res.status(403).json({ msj: 'Token invalido o expirado', status: 403 });
        }
    } else {
        res.status(403).json({ msj: 'Lo siento, no tienes permiso para acceder a este contenido.', status: 403 });
    }
}



//Show all orders of a user
const showUserOrders = async (req, res) => {
    let desToken = validateAdmin(req.headers.authorization);
    if (desToken != false) {
        let response = await userOrders(desToken);
        (response.length == 0) ? (response = 'Aun no has echo ningun pedido') : ('');
        res.status(200).json(response); //...
    } else {
        res.status(403).send('Token invalido o expirado');
    }
}




//Show all orders
const showAllOrders = (req, res) => {
    let desToken = validateAdmin(req.headers.authorization);
    if (desToken != false) {
        if (desToken.range == 'admin') {
            Order.find()
                .then(resolve => res.status(200).json(resolve))
                .catch(err => res.status(404).send(err));
        } else {
            res.status(403).send('Lo siento, no tienes permiso para acceder a este contenido.');
        }
    } else {
        res.status(403).send('Token invalido o expirado');

    }
}




export {
    newOrder,
    modOrder,
    cancelOrder,
    confOrder,
    changeStadeOrder,
    showUserOrders,
    showAllOrders
};