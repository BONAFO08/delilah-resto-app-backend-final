import "dotenv/config.js";
import { loginGoogle } from "../controllers/google.controller.js";

const loginWithGoogle = (req, res) => {
    loginGoogle({
        googleID: req.user.id,
        type: 1,
        name: `${req.user.name.givenName}${(req.user.name.familyName == undefined) ? ('') : (` ${req.user.name.familyName}`)}`,
        email: req.user.email,
        userImg: req.user.picture
    })
        .then(resolve => res.status(200)
        .redirect(`${process.env.FRONT_END_URL}/views/googleRedirect.html?data=token=${resolve.token}&&sessionID=${resolve.sessionID}`))
        .catch(err => res.status(400).json({ msj: err }));
        
}

export {
    loginWithGoogle
}