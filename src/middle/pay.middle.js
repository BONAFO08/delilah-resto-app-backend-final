import { validatePayData } from '../controllers/dataVerify.js';
import { validateAdmin } from '../controllers/user.controller.js'
import { createPay, modifyAPay, deletePay } from '../controllers/pay.controller.js';

//Create a new payment
const newPay = (req, res) => {
    let desToken = validateAdmin(req.headers.authorization);
    if (desToken != false) {
        if (desToken.range == 'admin') {
            let pay = validatePayData(req.body, '', 'new')
            if (pay.boolean == true) {
                createPay(pay.data)
                    .then(resolve => res.status(200).json({ msj: resolve, status: 200 }))
                    .catch(err => res.status(409).json({ msj: err, status: 409 }));
            } else {
                res.status(400).json({ msj: 'Lo siento.Haz enviado datos invalidos.', status: 400 });
            }
        } else {
            res.status(403).json({ msj: 'Lo siento, no tienes permiso para acceder este contenido.', status: 403 });
        }
    } else {
        res.status(403).json({ msj: 'Token invalido o expirado', status: 403 });
    }
}

//Modify a payment
const modPay = async (req, res) => {

    let desToken = validateAdmin(req.headers.authorization);

    if (desToken != false) {
        if (desToken.range == 'admin') {
            req.body['id'] = req.query.id;
            let pay = validatePayData(req.body, '', 'update');
            if (pay.name != false && pay.id != false) {
                let response = await modifyAPay(pay, pay.id);
                res.status(response.status).json({ msj: response.txt, status: response.status });
            } else {
                res.status(400).json({ msj: 'Lo siento.Haz enviado datos invalidos.', status: 400 });
            }
        } else {
            res.status(403).json({ msj: 'Lo siento, no tienes permiso para acceder a este contenido.', status: 403 });
        }
    } else {
        res.status(403).json({ msj: 'Token invalido o expirado', status: 403 });
    }
};



//Delete a payment
const delPay = async (req, res) => {

    let desToken = validateAdmin(req.headers.authorization);

    if (desToken != false) {
        if (desToken.range == 'admin') {
            let pay = validatePayData(req.query, '', 'update');
            if (pay.id != false) {
                let response = await deletePay(pay.id);
                res.status(response.status).json({ msj: response.txt, status: response.status });
            } else {
                res.status(400).json({ msj: 'Lo siento.Haz enviado datos invalidos.', status: 400 });
            }
        } else {
            res.status(403).json({ msj: 'Lo siento, no tienes permiso para acceder a este contenido.', status: 403 });
        }
    } else {
        res.status(403).json({ msj: 'Token invalido o expirado', status: 403 });
    }
};

export {
    newPay,
    modPay,
    delPay
}